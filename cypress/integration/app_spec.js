describe('App', () => {
    before(() => {
        cy.visit('http://localhost:3000');
    });

    it('toggles app to all wrong answers', () => {
        cy.get('[data-cy=ToggleSwitch]').eq(1).find('[data-cy=ToggleSwitchButton]').eq(1).click();
        cy.screenshot();
        cy.get('[data-cy=ToggleSwitch]').eq(2).find('[data-cy=ToggleSwitchButton]').eq(1).click();
        cy.screenshot();
    });

    it('toggles app to correct answers', () => {
        const correctAnswers = [1, 0, 0, 1];
        cy.get('[data-cy=ToggleSwitch]').each((toggleSwitch, index) => {
            cy.wrap(toggleSwitch).find('[data-cy=ToggleSwitchButton]').eq(correctAnswers[index]).click();
            cy.screenshot();
        });
    });

    it('can\'t toggle when all answers are correct', () => {
        cy.get('[data-cy=ToggleSwitch]').eq(0).find('[data-cy=ToggleSwitchButton]').eq(0).click();
        cy.get('[data-cy=AppAnswers]').invoke('text').then(text => {
            expect(text).to.eq('The answer is correct');
        });
    });

    it('changes the toggle switch direction in smaller viewports ', () => {
        cy.viewport(320, 1000);
        cy.get('[data-cy=ToggleSwitch]').first().trigger('resize');
        cy.get('[data-cy=ToggleSwitch]').last().should('have.class', 'ToggleSwitch--overflow');
    });
});