import { backgroundColours, answers } from './data';
import { SelectedAnswer } from './model/Answers';
import { getIsCorrectAnswer, getLinearGradientColours, getTotalNumberOfCorrectAnswers } from './utils';

describe('utils', () => {
    describe('getLinearGradientColours', () => {
        it('gets linear colours correctly', () => {
            const options = [0, .3, .5, .7, .9, 1];
            options.forEach((option, index) => {
                expect(getLinearGradientColours(option)).toEqual(backgroundColours[index]);
            });
        });
    });

    describe('getIsCorrectAnswer', () => {
        it('gets is correct answer correctly', () => {
            expect(getIsCorrectAnswer({id: answers[0].id, selectedAnswer: answers[0].correctAnswer}, answers)).toBeTruthy();
        });
    });

    describe('getTotalNumberOfCorrectAnswers', () => {
        it('gets total number of correct answers correctly', () => {
            const selectedAnswers: SelectedAnswer[] = [
                {id: answers[0].id, selectedAnswer: 'incorrect'},
                {id: answers[1].id, selectedAnswer: answers[1].correctAnswer},
                {id: answers[2].id, selectedAnswer: 'incorrect'},
                {id: answers[3].id, selectedAnswer: answers[3].correctAnswer},
            ];
            expect(getTotalNumberOfCorrectAnswers(selectedAnswers, answers)).toEqual(2);
        });
    });
});
