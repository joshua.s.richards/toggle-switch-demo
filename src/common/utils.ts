import { backgroundColours } from './data';
import { SelectedAnswer, Answer } from './model/Answers';

export const getLinearGradientColours = (percent: number): [string, string] => {
    if (percent < .2) {
        return backgroundColours[0];
    } else if (percent < .4) {
        return backgroundColours[1];
    } else if (percent < .6) {
        return backgroundColours[2];
    } else if (percent < .8) {
        return backgroundColours[3];
    } else if (percent < 1) {
        return backgroundColours[4];
    } else {
        return backgroundColours[5];
    }
}

export const getIsCorrectAnswer = (selectedAnswer: SelectedAnswer, answers: Answer[]): boolean => {
    return selectedAnswer.selectedAnswer === answers.find(answer => answer.id === selectedAnswer.id)?.correctAnswer;
}

export const getTotalNumberOfCorrectAnswers = (selectedAnswers: SelectedAnswer[], answers: Answer[]): number => {
    return selectedAnswers.reduce((total, answer) => getIsCorrectAnswer(answer, answers) ? ++total : total, 0);
}