import { useEffect, useState } from 'react';

export function useCheckOutOfBounds<T extends HTMLElement>(): [(node: T) => void, boolean] {
    const [width, setWidth] = useState(0);
    const [isOutOfBounds, setIsOutOfBounds] = useState(false);
    const ref = (node: T) => node && !width && setWidth(node.getBoundingClientRect().width);

    useEffect(() => {
        const checkResize = () => {
            setIsOutOfBounds(width >= (window.innerWidth || document.documentElement.clientWidth));
        }

        window.addEventListener('resize', checkResize);
        checkResize();

        return () => {
            window.removeEventListener('resize', checkResize);
        }
    }, [width]);

    return [ref, isOutOfBounds];
}
