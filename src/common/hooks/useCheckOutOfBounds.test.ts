import { act } from 'react-dom/test-utils';
import { testHook } from '../testingUtils/TestHook';
import { useCheckOutOfBounds } from './useCheckOutOfBounds';

let response: [(node: HTMLUListElement) => void, boolean];

describe('useCheckOutOfBounds', () => {
    it('sets out of bounds correctly', () => {
        testHook(() => response = useCheckOutOfBounds());

        expect(response[1]).toBeFalsy();

        window.innerWidth = 40;

        act(() => {
            response[0]({getBoundingClientRect: () => ({width: 50})});
            global.dispatchEvent(new Event('resize'));
        });

        expect(response[1]).toBeTruthy();
    });

    it('removes event listener on unmount', () => {
        window.removeEventListener = jest.fn().mockImplementation(window.removeEventListener);
        const container = testHook(() => response = useCheckOutOfBounds());

        act(() => {
            container.unmount();
        });

        expect(window.removeEventListener).toHaveBeenCalledWith('resize', expect.any(Function));
    });
});
