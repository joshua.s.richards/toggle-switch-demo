export interface Answer {
    id: number;
    options: string[];
    correctAnswer: string;
}

export interface SelectedAnswer {
    id: number;
    selectedAnswer: string;
}