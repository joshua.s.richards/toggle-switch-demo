import { FC } from 'react';
import { mount } from 'enzyme';

interface Props {
    callback: Function;
}

const TestHook: FC<Props> = ({ callback }) => {
    callback();
    return null;
}

export const testHook = (callback: Function) => mount(<TestHook callback={callback} />);
