import { Answer } from './model/Answers';

export const answers: Answer[] = [
    { id: 1, options: ['Cell Wall', 'Ribosomes'], correctAnswer: 'Ribosomes' },
    { id: 2, options: ['Cytoplasm', 'Chloroplast'], correctAnswer: 'Cytoplasm' },
    { id: 3, options: ['Partially permeable membrane', 'Impermeable membrane'], correctAnswer: 'Partially permeable membrane' },
    { id: 4, options: ['Cellulose', 'Mitochondria', 'Michrochondria'], correctAnswer: 'Mitochondria' },
];

export const backgroundColours: [string, string][] = [
    ['247, 59, 28', '250, 145, 97'],
    ['252, 115, 3', '252, 165, 3'],
    ['252, 165, 3', '252, 206, 3'],
    ['252, 206, 3', '252, 244, 3'],
    ['3, 202, 252', '3, 252, 231'],
    ['71, 228, 194', '7, 207, 221']
];
