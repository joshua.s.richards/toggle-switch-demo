import { useState, useMemo } from 'react';
import ToggleSwitch from './components/ToggleSwitch/ToggleSwitch';
import { getIsCorrectAnswer, getLinearGradientColours, getTotalNumberOfCorrectAnswers } from './common/utils';
import { SelectedAnswer } from './common/model/Answers';
import { answers } from './common/data';
import './App.css';

function App() {
  const [selectedAnswers, setSelectedAnswers] = useState<SelectedAnswer[]>(answers.map(element => ({ id: element.id, selectedAnswer: element.options[0] })));
  const backgroundColours = useMemo(() => getLinearGradientColours(
    getTotalNumberOfCorrectAnswers(selectedAnswers, answers) / answers.length),
    [selectedAnswers]);

  const _setSelectedOption = (id: number, selectedAnswer: string): void => {
    setSelectedAnswers(selectedAnswers => selectedAnswers.map(option => {
      if (option.id === id) {
        return { ...option, selectedAnswer };
      }

      return option;
    }));
  }

  const _areAllCorrect = (): boolean => {
    return selectedAnswers.every(option => getIsCorrectAnswer(option, answers));
  }

  return (
    <div className="App" style={{ backgroundImage: `linear-gradient(to bottom, rgb(${backgroundColours[0]}), rgb(${backgroundColours[1]}))` }}>
      <main>
        {answers.map(element => (
          <ToggleSwitch
            key={element.id}
            options={element.options}
            onToggle={(selectedOption: string) => _setSelectedOption(element.id, selectedOption)}
            canBeToggled={!_areAllCorrect()}
          />
        ))}
        <b className="App__answers" data-cy="AppAnswers">The answer is {_areAllCorrect() ? 'correct' : 'incorrect'}</b>
      </main>
    </div>
  );
}

export default App;
