import { shallow } from 'enzyme';
import App from './App';
import ToggleSwitch from './components/ToggleSwitch/ToggleSwitch';

const getContainer = () => shallow(<App/>);

describe('App', () => {
  it('renders correctly', () => {
    expect(getContainer()).toMatchSnapshot();
  });

  it('gets all answers correct', () => {
    const container = getContainer();

    expect(container.find('b.App__answers').text()).toEqual('The answer is incorrect');

    container.find(ToggleSwitch).at(0).props().onToggle('Ribosomes');
    container.find(ToggleSwitch).at(3).props().onToggle('Mitochondria');

    expect(container.find('b.App__answers').text()).toEqual('The answer is correct');
  });
});
