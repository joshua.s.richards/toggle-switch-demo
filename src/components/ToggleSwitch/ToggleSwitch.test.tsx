import { shallow } from 'enzyme';
import ToggleSwitch from './ToggleSwitch';
import { ToggleSwitchProps } from './ToggleSwitchProps';
import * as resizeHook from '../../common/hooks/useCheckOutOfBounds';

const defaultProps: ToggleSwitchProps = {
    options: ['option1', 'option2'],
    onToggle: jest.fn(),
    canBeToggled: true,
}

const getContainer = (props: ToggleSwitchProps = defaultProps) => shallow(<ToggleSwitch {...props}/>);

describe('ToggleSwitch', () => {
  it('renders correctly', () => {
    expect(getContainer()).toMatchSnapshot();
  });

  it('toggles correctly', () => {
    const onToggle = jest.fn();
    const container = getContainer({...defaultProps, onToggle});

    expect(container.find('button.ToggleSwitch__option__button').at(0).hasClass('ToggleSwitch__option__button--selected')).toBeTruthy();
    expect(container.find('button.ToggleSwitch__option__button').at(1).hasClass('ToggleSwitch__option__button--selected')).toBeFalsy();


    container.find('button.ToggleSwitch__option__button').at(1).props().onClick();

    expect(onToggle).toHaveBeenCalledTimes(1);
    expect(onToggle).toHaveBeenCalledWith(defaultProps.options[1]);
    expect(container.find('button.ToggleSwitch__option__button').at(0).hasClass('ToggleSwitch__option__button--selected')).toBeFalsy();
    expect(container.find('button.ToggleSwitch__option__button').at(1).hasClass('ToggleSwitch__option__button--selected')).toBeTruthy();  
});

  it('can\'t toggle if canBeToggled is false', () => {
    const onToggle = jest.fn();
    const container = getContainer({...defaultProps, onToggle, canBeToggled: false});

    expect(container.find('button.ToggleSwitch__option__button').at(0).hasClass('ToggleSwitch__option__button--selected')).toBeTruthy();

    container.find('button.ToggleSwitch__option__button').at(1).props().onClick();

    expect(onToggle).not.toHaveBeenCalled();
    expect(container.find('button.ToggleSwitch__option__button').at(0).hasClass('ToggleSwitch__option__button--selected')).toBeTruthy();
  });

  it('gets background styling correctly', () => {
    resizeHook.useCheckOutOfBounds = jest.fn().mockReturnValue([null, false]);
    let container = getContainer();

    expect(container.find('li.ToggleSwitch__background').props().style).toEqual({
        transform: `translateX(0%)`,
        width: `${(1 / defaultProps.options.length) * 100}%`
    });

    resizeHook.useCheckOutOfBounds = jest.fn().mockReturnValue([null, true]);
    container = getContainer();

    container.find('button.ToggleSwitch__option__button').at(1).props().onClick();

    expect(container.find('li.ToggleSwitch__background').props().style).toEqual({
        transform: `translateY(100%)`,
        height: `${(1 / defaultProps.options.length) * 100}%`,
        width: '100%',
    });
  });
});