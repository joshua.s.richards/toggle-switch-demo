import { FC, useState } from 'react';
import { useCheckOutOfBounds } from '../../common/hooks/useCheckOutOfBounds';
import { ToggleSwitchProps } from './ToggleSwitchProps';
import './ToggleSwitch.css';

const ToggleSwitch: FC<ToggleSwitchProps> = ({ options, onToggle, canBeToggled }) => {
    const [ref, isOutOfBounds] = useCheckOutOfBounds<HTMLUListElement>();
    const [selectedValue, setSelectedValue] = useState(options[0]);

    const percentTransform = (options.indexOf(selectedValue) / (options.length - 1)) * 100 * (options.length - 1);
    const backgroundStyle = isOutOfBounds ? {
        transform: `translateY(${percentTransform}%)`,
        height: `${(1 / options.length) * 100}%`,
        width: '100%',
    } : {
        transform: `translateX(${percentTransform}%)`,
        width: `${(1 / options.length) * 100}%`,
    };

    const _getButtonClassName = (option: string): string => {
        return `ToggleSwitch__option__button ${option === selectedValue ? 'ToggleSwitch__option__button--selected' : ''}`;
    }

    const _onToggle = (option: string): void => {
        if (canBeToggled) {
            setSelectedValue(option);
            onToggle(option);
        }
    }

    return (
        <ul className={`ToggleSwitch ${isOutOfBounds ? 'ToggleSwitch--overflow' : ''}`} ref={ref} data-cy="ToggleSwitch">
            {options.map(option => (
                <li key={option} className="ToggleSwitch__option">
                    <button className={_getButtonClassName(option)} onClick={() => _onToggle(option)} data-cy="ToggleSwitchButton">
                        {option}
                    </button>
                </li>
            ))}
            <li className="ToggleSwitch__background" style={backgroundStyle} />
        </ul>
    );
}

export default ToggleSwitch;