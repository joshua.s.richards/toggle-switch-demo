export interface ToggleSwitchProps {
    options: string[];
    onToggle: (option: string) => void;
    canBeToggled: boolean;
}
